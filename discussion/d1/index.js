// npm init
/* 
 - prompt diff settings that will define the app
 - will make a "package.json in our repo"
 - package.json tracks the version of our app, depending on the setting we have set.
 - also, we can see the dependencies that we have installed inside of this file
*/



// 1. creating a server with ExpressJS using these codes:

const express = require("express"); 

const app = express(); //<== creates server using express, the app variable is our server

const port = 3000; // <== set up port variable

app.use(express.json()); // <== let server read/handle json data from requests
// middleware methods
app.use(express.urlencoded({extended:true})); // <= reads data from forms (using extended:true allows other data types instead of only strings/array)

//GET method ; the route expects receive a GET request
app.get("/", (req, res) => {  
  res.send("Hello World");
});
app.get("/hello", (req, res) => {  
  res.send("Hello from /hello endpoint");
});

//POST method
app.post("/hello", (req, res) => {  
  res.send(`Hello World ${req.body.firstName} ${req.body.lastName} ${req.body.age}`);
});

let users = [];
app.post("/signup", (req, res) => {  
  if (req.body.firstName !== "" && req.body.lastName !== ""){
    users.push(req.body);
    res.send("User signed up successfully!");
    console.log(users);
} else {
  res.send("please fill in information");
}
}
);
// 

//PUT method
app.put("/change-lastName", (req, res) => {  
  console.log(users);
  let message;
  for(let i = 0; i<users.length; i++){
    if(req.body.firstName == users[i].firstName){
      users[i].lastName = req.body.lastName;
      message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
    break;
  } else {
    message = "user does not exist";
  }}
  res.send(message);
});


// 2. add start script "start": "nodemon index.js"
// then "npm start" in terminal

// ^^^^^ ACTIVITY

// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.

app.get("/home", (req, res) => {  
  res.send("Welcome to our Home Page!");
});

// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.



  // users = [{
  //   "firstName": "Steve",
  //   "lastName": "Moore",
  //   "age": 31
  // }];

app.get("/users", (req, res) => {  
  res.send(users);
});

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.
// 7. Export the Postman collection and save it inside the root folder of our application.

  // users = [{
  //   "firstName": "Steve",
  //   "lastName": "Moore",
  //   "age": 31
  // }];

  app.delete('/delete-user', (req, res) => {
    res.send("A user has been deleted")
  })

// app.delete("/delete-user", (req, res) => {  
//   res.send(`$req.body`);
// });

// app.delete("/delete-user", (req, res) => {  
//   let message;
//   for(let i = 0; i<users.length; i++){
//     if(req.body.firstName == users[i].firstName){
//       users[i].lastName = req.body.lastName;
//       message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
//     break;
//   } else {
//     message = "user does not exist";
//   }}
//   res.send(message);
// });


app.listen(port, () => console.log(`Server running at port ${port}`)); //tells our server to listen to port/ if port is accessed, we run the server// returns a message to confirm


